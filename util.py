import logging
import logging.config
import yaml
import datetime
from pytz import timezone


class TZFormatter(logging.Formatter):
    converter = lambda *args: datetime.datetime.now(timezone("Asia/Tokyo")).timetuple()


def logging_config(config_path):
    with open(config_path) as file:
        logging_config = yaml.safe_load(file.read())
        for formatter in logging_config["formatters"].values():
            formatter["()"] = TZFormatter
        logging.config.dictConfig(logging_config)
